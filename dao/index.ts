import Pricing from "./pricing"

export type RetailPricingService = {
  getPrice(_: ProductId): PriceInCent
}

export type TargetingService = {
  pricingRulesForCustomer(id: CustomerId): Pricing[]
}

/* 

basically to implement the described functionality we need 2 sets of data:
the pricing data and the customer targeting data.

We don't do any database here so I wont go into normalising them 

*/