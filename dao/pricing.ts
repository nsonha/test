module Pricing {
  export class FixedDiscount {
    constructor(
      readonly productId: ProductId,
      public discountedPrice: PriceInCent
    ) {}
  }

  export class FreeDiscount {
    constructor(
      public condition: Readonly<{
        productId: ProductId,
        quantity: number
      }>,
      public freeQuantity?: number
    ) {}
  }
}

type Type = keyof typeof Pricing

type Pricing = {
  [T in Type]:
  & InstanceType<typeof Pricing[T]>
  & { type: T }
}[Type]

export default Pricing
