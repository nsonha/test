import { execSync } from 'child_process'

test.each([
  ['default', { 'Small Pizza': 1, 'Medium Pizza': 1, 'Large Pizza': 1 }, 49.97],
  ['Microsoft', { 'Small Pizza': 3, 'Large Pizza': 1 }, 45.97],
  ['Amazon', { 'Medium Pizza': 3, 'Large Pizza': 1 }, 67.96]
])("%s %o", (customerId, items, total) => {
  const stdout = execSync(
    `node_modules/.bin/ts-node --transpile-only app ${
      customerId} '${JSON.stringify(items)
    }'`
  )
  
  expect(stdout.toString().trim()).toBe(String(total))
})
