import {
  RetailPricingService,
  TargetingService,
  PricingService,
  Checkout
} from './services'

export default class AppContainer {
  constructor(readonly services: {
    retail: RetailPricingService
    targeting: TargetingService
  }) {}

  createCheckout(id: CustomerId) {
    const pricing = new PricingService(
      this.services.targeting,
      this.services.retail
    )
    
    return new Checkout(pricing.rulesForCustomer(id))
  }
}

const container = new AppContainer({
  retail: {
    getPrice: id => ({
      'Small Pizza': 11_99,
      'Medium Pizza': 15_99,
      'Large Pizza': 21_99,
    })[id]
  },
  targeting: {
    pricingRulesForCustomer: id => {
      const rule = ({
        Microsoft: {
          type: 'FreeDiscount',
          condition: { productId: 'Small Pizza', quantity: 2 }
        },
        Amazon: {
          type: 'FixedDiscount',
          productId: 'Large Pizza',
          discountedPrice: 19_99 
        },
        Facebook: {
          type: 'FreeDiscount',
          condition: { productId: 'Small Pizza', quantity: 4 }
        },
      } as const)[id]
      
      return rule ? [ rule ] : []
    }
  }
})

const [ clientId, json ] = process.argv.slice(2)
const checkout = container.createCheckout(clientId)

const quantitiesById = JSON.parse(json)
Object.keys(quantitiesById).forEach(id => {
  let quantity = quantitiesById[id]
  while (quantity--) checkout.add(id)
})

console.log((checkout.total() / 100).toFixed(2))
