type ProductId = string
type PriceInCent = number
type CustomerId = string

// this is the item in the shopping cart
type LineItem = {
  id: ProductId
  quantity: number
}

// There are more: Product, ProductPricing, Targeting etc, but they're not really reallevant to the scope of this excercise
