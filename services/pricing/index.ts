import RuleFactory, { PricingRule } from './rules'
import { RetailPricingService, TargetingService } from '../../dao'
import { RetailPricingRule } from './rules'

type PricingService = {
  rulesForCustomer(_: CustomerId): PricingRule[]
}

class CustomerPricingService implements PricingService {
  private ruleFactory

  constructor(
    private targetingService: TargetingService,
    private retailPricingService: RetailPricingService
  ) {
    this.ruleFactory = new RuleFactory(retailPricingService)
  }

  rulesForCustomer(id: CustomerId) {
    return this.targetingService
      .pricingRulesForCustomer(id)
      .map(pricing => this.ruleFactory.create(pricing))
      .concat(new RetailPricingRule(this.retailPricingService))
  }
}

export { PricingRule }
export default CustomerPricingService
