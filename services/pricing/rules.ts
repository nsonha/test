import { RetailPricingService } from '../../dao'
import Pricing from '../../dao/pricing'

export type PricingRule = {
  apply(_: LineItem[]): ApplyRuleResult
}

type ApplyRuleResult = PricingResult | NotApplicable

type PricingResult = {
  items: LineItem[]
  total: PriceInCent
/* 
  if we want to be really careful about async boundary, then we can separate 
  the apply() logic which is pure from the pricing and make total a function:

  total(_: RetailPricingService): Promise<number>
*/
}

type NotApplicable = void

export class FreeDiscountRule implements PricingRule {
  constructor(
    private pricing: Pricing.FreeDiscount,
    private pricingService: RetailPricingService
  ) {}
  
  apply(items: LineItem[]) {
    const { productId, quantity } = this.pricing.condition

    const lineItem = items
      .find(lineItem =>
        (lineItem.id == productId) &&
        (lineItem.quantity >= quantity)
      )

    if (!lineItem) return

    const factor = Math.floor(lineItem.quantity / quantity)
    const toCharge = factor * quantity
    const freeEligible = factor * (this.pricing.freeQuantity ?? 1)

    return {
      items: [
        {
          id: lineItem.id,
          quantity: toCharge + Math.min(lineItem.quantity - toCharge, freeEligible)
        }
      ],
      total: toCharge * this.pricingService.getPrice(productId)
    }
  }
}

export class FixedDiscountRule implements PricingRule {
  constructor(private pricing: Pricing.FixedDiscount) {}

  apply(items: LineItem[]) {
    const { pricing: discount } = this

    if (items.some(({ id }) => id == discount.productId)) return {
      items: [{ id: discount.productId, quantity: 1 }],
      total: discount.discountedPrice
    }
  }
} 

export class RetailPricingRule implements PricingRule {
  constructor(private pricingService: RetailPricingService) {}

  apply = (items: LineItem[]) => ({
    items,
    total: items.reduce(
      (sum, { id, quantity }) =>
        sum + quantity * this.pricingService.getPrice(id)
      , 0
    )
  }) 
}

class RuleFactory {
  constructor(private pricingService: RetailPricingService) {}
  
  create(pricing: Pricing): PricingRule {
    const RULES = {
      FixedDiscount: FixedDiscountRule,
      FreeDiscount: FreeDiscountRule
    }

    const constructor = RULES[pricing.type]
    if (!constructor) throw new RuleFactory.InvalidPricingModel(pricing)

    return new constructor(pricing as any, this.pricingService)
  }
}

module RuleFactory {
  export class InvalidPricingModel extends Error {
    constructor(model: any) {
      super('invalid pricing model object: ' + JSON.stringify(model))
    }
  }
}

export default RuleFactory
