import Factory, { FreeDiscountRule, FixedDiscountRule } from './rules'

describe('FreeDiscountRule', () => {
  const MOCK_PRICE = 3
  const getPrice = jest.fn(() => MOCK_PRICE)
  
  const discount = new FreeDiscountRule(
    {
      condition: {
        productId: 'Small Pizza',
        quantity: 2
      },
      freeQuantity: 1,
    },
    { getPrice }
  )

  afterEach(jest.clearAllMocks)

  test.each`message | lineItems | expected
    ${'return falsy if there is no line item with matching productId'}
    ${[ { id: 'Medium Pizza', quantity: 5 } ]}
    ${undefined}
    
    ${'return falsy if not enough quantity'}
    ${[ { id: 'Medium Pizza', quantity: 1 } ]}
    ${undefined}

    ${'apply() for the same quantity if exist a line item with exact quantity and productId'}
    ${[ { id: 'Small Pizza', quantity: 2 } ]}
    ${{
      items: [ { id: 'Small Pizza', quantity: 2 }], 
      total: MOCK_PRICE * 2 }}

    ${'apply() for the quantity in "condition" plus "freeQuantity" if line item has quantity greater than that of the condition'}
    ${[ { id: 'Small Pizza', quantity: 3 } ]}
    ${{
      items: [ { id: 'Small Pizza', quantity: 3 }], 
      total: MOCK_PRICE * 2 }}

    ${'apply() for a multiplication of the given quantity'}
    ${[ { id: 'Small Pizza', quantity: 7 } ]}
    ${{
      items: [ { id: 'Small Pizza', quantity: 7 }], 
      total: MOCK_PRICE * 6 }}

  `('should $message', ({ lineItems, expected }) => {
    const result = expect(discount.apply(lineItems))

    if (expected === undefined) result.toBeUndefined()
    else result.toEqual(expected)
  })

  it('should not call getPrice() if not applicable', () => {
    expect(
      discount.apply([
        { id: 'Large Pizza', quantity: 5 },
        { id: 'Medium Pizza', quantity: 5 }
      ])
    ).toBeFalsy()

    expect(getPrice).not.toHaveBeenCalled()
  })

  it('should call getPrice() only once if applicable', () => {
    expect(
      discount.apply([
        { id: 'Small Pizza', quantity: 5 },
        { id: 'Medium Pizza', quantity: 5 }
      ])
    ).toBeTruthy()

    expect(getPrice.mock.calls).toEqual([[ 'Small Pizza' ]])
  })
})

describe('FixedDiscountRule', () => {
  const MOCK_PRICE = 3

  const discount = new FixedDiscountRule({
    productId: 'Small Pizza',
    discountedPrice: MOCK_PRICE
  })
  
  test.each`message | lineItems | expected
    ${'return falsy if ther is no matching product'}
    ${[ { id: 'Medium Pizza', quantity: 5 } ]}
    ${undefined}

    ${'apply() for exact one matching item'}
    ${[ { id: 'Small Pizza', quantity: 1 } ]}
    ${{
      items: [ { id: 'Small Pizza', quantity: 1 }], 
      total: MOCK_PRICE }}

    ${'apply() for exact one item out of more than one matching items'}
    ${[ { id: 'Small Pizza', quantity: 7 }, { id: 'Medium Pizza', quantity: 5 } ]}
    ${{
      items: [ { id: 'Small Pizza', quantity: 1 }], 
      total: MOCK_PRICE }}
  `('should $message', ({ lineItems, expected }) => {
    const result = expect(discount.apply(lineItems))

    if (expected === undefined) result.toBeUndefined()
    else result.toEqual(expected)
  })
})

describe('RuleFactory', () => {
  const retailPricingService = { getPrice: jest.fn() }
  const factory = new Factory(retailPricingService)
  
  it('FixedDiscount', () => {
    const pricing = { productId: 'Small Pizza', discountedPrice: 10 }
    const result = expect(
      factory.create(
        { type: 'FixedDiscount', ...pricing }
      )
    )
    
    result.toBeInstanceOf(FixedDiscountRule)
    result.toMatchObject({ pricing })
  })

  it('FreeDiscount', () => {
    const pricing = {
      condition: {
        productId: 'Small Pizza',
        quantity: 2
      }
    }
    
    const result = expect(
      factory.create({ type: 'FreeDiscount', ...pricing })
    )
    
    result.toBeInstanceOf(FreeDiscountRule)
    result.toMatchObject({ pricing })
    result.toHaveProperty('pricingService', retailPricingService)
  })

  it('InvalidPricingModel', () => {
    const invalid: any = { invalid: 'object' }
    expect(() => factory.create(invalid)).toThrow(
      'invalid pricing model object: {"invalid":"object"}'
    )
  })
})
