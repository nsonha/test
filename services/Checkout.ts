import { PricingRule } from './pricing'
import { RetailPricingRule } from './pricing/rules'

const subtract = (from: LineItem[], toSubstract: LineItem[]) => {
  const quantityById: Record<ProductId, number> = toSubstract.reduce(
    (map, { id, quantity }) => ({ ...map, [id]: quantity }),
    {}
  )

  return from
    .map(({ id, quantity }) => {
      const subtractBy = quantityById[id] ?? 0
      quantity -= subtractBy
      if (quantity < 0) throw Error(JSON.stringify({ quantity }))
      return { id, quantity }
    })
    .filter(it => it.quantity)
}

class Checkout {
  private lineItems = Array<LineItem>()

  constructor(private pricingRules: PricingRule[]) {}

  getPriceBreakdown() {
    type PricingGroup = {
      pricingRule: PricingRule
      items: LineItem[]
      total: number
    }

    const { breakdown: groups, remaining: { length } } = this.pricingRules
      .reduce<{ remaining?: LineItem[], breakdown: PricingGroup[] }>(
        ({ remaining = this.lineItems, breakdown }, pricingRule) => {
          const match = pricingRule.apply(remaining)
          if (!match) return { remaining, breakdown }

          remaining = subtract(remaining, match.items)
          breakdown = [
            ...breakdown,
            { ...match, pricingRule }
          ]

          return { remaining, breakdown }
        },
        { breakdown: [] }
      )

    if (length) throw new Checkout.PricingRuleError(this.pricingRules, this.lineItems)

    return groups
  }

  // TODO verify productId
  add(item: ProductId) {
    const lineItem = this.lineItems.find(({ id }) => id == item)

    if (lineItem) lineItem.quantity++
    else this.lineItems.push({ id: item, quantity: 1 })
  }

  total() {
    const breakdown = this.getPriceBreakdown()

    return breakdown.reduce((sum, { total }) => sum + total, 0)
  }
}

module Checkout {
  export class PricingRuleError  {
    constructor(readonly rules: PricingRule[], readonly lineItems: LineItem[]) {
      const obj = rules[rules.length - 1]
      const { constructor } = obj

      this.toString = () => `
        Atempt to apply pricing rules resulted in unapplied line items
        This usually happens if a ${RetailPricingRule.name} is NOT present at the end of the rule list.

        the last rule in the list is ${constructor.name} ${JSON.stringify(obj, null, 2)}
      `

      // Leaky abstraction? maybe. In the initial implementation I had the RetailPricingRule baked in Checkout, the trade-off was that Checkout then had more dependencies than just the passed rules (it needed to know about RetailPricingService).
    }
  }
}

export default Checkout
