import Checkout from './Checkout'
import { FixedDiscountRule, FreeDiscountRule, RetailPricingRule } from "./pricing/rules"

describe('Checkout', () => {
  const PRICE = {
    'Small Pizza': 21_99,
    'Medium Pizza': 15_99,
    'Large Pizza': 21_99,
  }

  const getPrice = jest.fn((id: keyof typeof PRICE) => PRICE[id])

  const discountedPrice = 19_99
  const defaultCheckout = new Checkout(
    [
      new FreeDiscountRule(
        {
          condition: {
            productId: 'Small Pizza',
            quantity: 2
          },
          freeQuantity: 1,
        },
        { getPrice }
      ),
      new FixedDiscountRule({
        productId: 'Large Pizza',
        discountedPrice
      }),
      new FreeDiscountRule(
        {
          condition: {
            productId: 'Medium Pizza',
            quantity: 4
          },
          freeQuantity: 1
        },
        { getPrice }
      ),
      new RetailPricingRule({ getPrice })
    ]
  )
  
  defaultCheckout.add('Small Pizza')
  defaultCheckout.add('Small Pizza')
  defaultCheckout.add('Small Pizza')
  defaultCheckout.add('Medium Pizza')
  defaultCheckout.add('Large Pizza')

  it('getPriceBreakdown()', () => {
    expect(defaultCheckout.getPriceBreakdown()).toMatchObject(
      [
        {
          items: [
            {
              id: 'Small Pizza',
              quantity: 3
            }
          ],
          pricingRule: {
            __proto__: FreeDiscountRule,
            pricing: {
              condition: {
                productId: 'Small Pizza',
                quantity: 2
              },
              freeQuantity: 1
            }
          },
          total: PRICE['Small Pizza'] * 2
        },
        {
          items: [
            {
              id: 'Large Pizza',
              quantity: 1
            }
          ],
          pricingRule: {
            __proto__: FixedDiscountRule,
            pricing: {
              discountedPrice,
              productId: 'Large Pizza'
            }
          },
          total: discountedPrice
        },
        { 
          items: [
            {
              id: 'Medium Pizza',
              quantity: 1,
            },
          ],
          pricingRule: { __proto__: RetailPricingRule },
          total: PRICE['Medium Pizza']
        }
      ]
    )
  })

  it('total()', () => {
    expect(defaultCheckout.total()).toBe(
      PRICE['Small Pizza'] * 2 +
      PRICE['Medium Pizza'] +
      discountedPrice
    )
  })

  it('PricingRuleError', () => {
    const checkout = new Checkout(
      [
        new FreeDiscountRule(
          {
            condition: {
              productId: 'Small Pizza',
              quantity: 2
            },
            freeQuantity: 1,
          },
          { getPrice }
        )
      ]
    )
    
    checkout.add('Medium Pizza')
    checkout.add('Large Pizza')

    expect(() => checkout.getPriceBreakdown()).toThrow(Checkout.PricingRuleError)
  })
})