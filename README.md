# Note

- I didn't use async because I kind of forgot, but it's not that important for this excercise
- I try to have dependency flow from data -> servcies -> app and avoid cycle with inversion of control
- There is a question about whether the service dependencies should be on pricing rules or Checkout.
  I was influenced by the code example so I put them on the pricing rules, just so Checkout interface looks clean. However, on second thought. I think it makes more sense for the checkout component to depend on services and the pricing rules should be pure logic
- There should be more edge case handling, it's just I had spent too much time already before I got to that point
